# Rust playground

Playground starts at `src/main.rs`

Compile by `$ rustc src/main.rs`
([Rust](https://www.rust-lang.org/tools/install) environment required)

Run by `$ ./main`

Enjoy :)
